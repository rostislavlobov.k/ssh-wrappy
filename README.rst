.. -*- coding: utf-8; indent-tabs-mode:nil; -*-
.. SPDX-FileCopyrightText: 2019-2021 Jérôme Carretero <cJ@zougloub.eu>
.. SPDX-License-Identifier: GPL-3.0-only

##########
ssh-wrappy
##########



.. contents::


Introduction
############

ssh-wrappy is an ssh wrapping tool that allows to use dynamic configuration
(via command-line arguments to the underlying ssh).

Basically, it's dangerous but helpful.


License
#######

GPLv3, see LICENSES/GPL-3.0-only.txt file.


Usage
#####

.. code:: sh

   ssh ...



Installation
############

Clone this repository eg. to `~/.local/opt/ssh-wrappy`.
Add to PATH or symlink `bin/ssh` to `~/.local/bin`.
Create `~/.config/ssh-wrappy`.


Configuration
#############

Write a plug-in in `~/.config/ssh-wrappy`, which is a python module
containing hook functions.

For example, `def hook_add_commandline_options(real_ssh, args):` which
may return a new command line (list) if it matches.

