#!/usr/bin/env python3
# -*- coding:utf-8 vi:noet
# SSH Wrapper that makes dynamic options
# PYTHON_ARGCOMPLETE_OK
# SPDX-FileCopyrightText: 2019-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import sys, io, os
import logging
import types
import shlex
import argparse
import hashlib


logger = logging.getLogger()


def list2cmdline(lst):
	return " ".join(shlex.quote(x) for x in lst)


def signature(cmd, env=None):
	h = hashlib.md5()
	for x in cmd:
		h.update(x.encode("utf-8"))
		h.update(b"\x00")
	if env is None:
		env = os.environ
	for k, v in sorted(env.items()):
		if k == "SSH_WRAPPY_SIGNATURE":
			continue
		h.update(k.encode("utf-8"))
		h.update(b"\x00")
		h.update(v.encode("utf-8"))
		h.update(b"\x00")
	return h.hexdigest()


def is_exe(fpath: str) -> bool:
	return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

"""
In some cases the PATH has been modified and the real tool
wouldn't be reachable with it; we still want reasonable fallback
in this case, so provide a hard-coded fallback list.
This situation arises, for example, when using OE Bitbake.
"""
FALLBACK_PATH = ["/usr/bin"]

def find_next_program(name: str):
	"""
	:param name: program basename
	:return: path of second match in PATH
	"""
	first = None

	for path in os.environ["PATH"].split(os.pathsep):
		exe_file = os.path.normpath(os.path.join(path, name))
		if is_exe(exe_file):
			if first is None:
				first = exe_file
				logger.debug("Ignoring first PATH instance of %s: %s", name, exe_file)
				continue
			elif exe_file == first:
				logger.debug("Ignoring redundant PATH instance of %s: %s", name, exe_file)
				continue
			return exe_file

	for d in FALLBACK_PATH:
		p = os.path.join(d, name)
		if os.path.exists(p):
			logger.debug("Using %s not in PATH at %s", name, p)
			return p


append_const_options = {
 "ssh": "246AaCfGgKkMNnqsTtVvXxYy",
 "scp": "346BCpqrTvdft",
 "ssh-copy-id": "fn",
}

regular_options = {
 "ssh": "BbcDEeFIiJlmOpQSWw",
 "scp": "cFiJlPS",
 "ssh-copy-id": "ip",
}

append_options = {
 "ssh": "oLR",
 "scp": "o",
 "ssh-copy-id": "o",
}


def unparse(tool, args):
	"""
	Rebuild a command-line from args
	"""

	logger.debug("Unparsing %s", args)

	ret = []

	# store_true
	for option in append_const_options[tool]:
		for a in getattr(args, option):
			ret.append("-{}".format(option))

	# regular
	for option in regular_options[tool]:
		v = getattr(args, option)
		if v is not None:
			ret.append("-{}".format(option))
			ret.append(v)

	# append - SSH takes only the first occurrence
	done = set()
	for option in append_options[tool]:
		for x in getattr(args, option):
			if (option,x) in done:
				continue
			ret.append("-{}".format(option))
			ret.append(x)
			done.add((option,x))

	if tool == "ssh":
		if args.destination:
			ret.append(args.destination)

		for x in args.command:
			ret.append(x)

	if tool == "scp":
		for x in args.source:
			ret.append(x)

		ret.append(args.target)

	if tool == "ssh-copy-id":
		ret.append(args.destination)

	return ret


def get_parser(tool):

	parser = argparse.ArgumentParser(
	 description="Wrapper for {}".format(tool),
	)

	# "undocumented" options
	for option in append_const_options[tool]:
		parser.add_argument("-{}".format(option),
		 const=True,
		 action="append_const",
		 default=[],
		)


	if tool == "ssh":
		parser.add_argument("-B",
		 metavar="[bind_interface]",
		)

		parser.add_argument("-b",
		 metavar="[bind_address]",
		)

	if tool in ("ssh", "scp"):
		parser.add_argument("-c",
		 metavar="[cipher_spec]",
		)

	if tool == "ssh":
		parser.add_argument("-D",
		 metavar="[[bind_address:]port]",
		)

		parser.add_argument("-E",
		 metavar="[log_file]",
		)

		parser.add_argument("-e",
		 metavar="[escape_char]",
		)

	if tool in ("ssh", "scp"):
		parser.add_argument("-F",
		 metavar="[configfile]",
		)

	if tool == "ssh":
		parser.add_argument("-I",
		 metavar="[pkcs11]",
		)

	if tool in ("ssh", "scp", "ssh-copy-id"):
		parser.add_argument("-i",
		 metavar="[identity_file]",
		)

	if tool in ("ssh", "scp"):
		parser.add_argument("-J",
		 metavar="[destination]",
		)

	if tool == "ssh":
		parser.add_argument("-L",
		 metavar="[address]",
		 action="append",
		 default=[],
		)

		parser.add_argument("-l",
		 metavar="[login_name]",
		)

	if tool == "scp":
		parser.add_argument("-l",
		 metavar="[limit]",
		)

	if tool == "ssh":
		parser.add_argument("-m",
		 metavar="[mac_spec]",
		)

		parser.add_argument("-O",
		 metavar="[ctl_cmd]",
		)

	if tool in ("ssh", "scp", "ssh-copy-id"):
		parser.add_argument("-o",
		 metavar="[option]",
		 action="append",
		 default=[],
		)

	if tool in ("ssh", "ssh-copy-id"):
		parser.add_argument("-p",
		 metavar="[port]",
		)

	if tool == "scp":
		parser.add_argument("-P",
		 metavar="[port]",
		)

	if tool == "ssh":
		parser.add_argument("-Q",
		 metavar="[query_option]",
		)

		parser.add_argument("-R",
		 metavar="[address]",
		 action="append",
		 default=[],
		)

	if tool == "scp":
		parser.add_argument("-S",
		 metavar="[program]",
		)

	if tool == "ssh":

		parser.add_argument("-S",
		 metavar="[ctl_path]",
		)

	if tool == "ssh":

		parser.add_argument("-W",
		 metavar="[host:port]",
		)

		parser.add_argument("-w",
		 metavar="[local_tun[:remote_tun]]",
		)

		parser.add_argument("destination",
		 nargs="?",
		)

		parser.add_argument("command",
		 metavar="[command]",
		 nargs=argparse.REMAINDER,
		)

	if tool == "scp":

		parser.add_argument("source",
		 metavar="[source]",
		 nargs="*", # not + because of -f and -t options
		)

		parser.add_argument("target",
		 metavar="[target]",
		)

	if tool == "ssh-copy-id":
		parser.add_argument("destination",
		)

	return parser


def main():

	tool = os.path.basename(sys.argv[0])
	tool_uc = tool.replace("-", "_").upper()
	tool_args = list2cmdline(sys.argv[1:])

	parser = get_parser(tool)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	log_level = os.environ.get("{}_LOG_LEVEL".format(tool_uc), "INFO")

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	logger.debug("Called %s with %s", tool, tool_args)

	caller_signature = os.environ.get("SSH_WRAPPY_SIGNATURE")
	if caller_signature:
		logger.debug("Caller signature: %s", caller_signature)

	xdg_config_home = os.environ.get("XDG_CONFIG_HOME", os.path.expanduser("~/.config"))
	hooks_root = os.path.join(xdg_config_home, "ssh-wrappy")

	cmd = sys.argv

	if args.destination is not None and "@" in args.destination:
		user, host = args.destination.split("@", 1)
		# append (lower priority)
		args.o += ["User={}".format(user)]
		args.destination = host

	for hook_fn in sorted(os.listdir(hooks_root)):
		if not hook_fn.endswith(".py"):
			continue

		hook_path = os.path.join(hooks_root, hook_fn)
		name = os.path.splitext(hook_fn)[0]

		if not os.access(hook_path, os.O_RDONLY):
			continue

		try:
			with io.open(hook_path, "rb") as f:
				code_data = f.read()
		except Exception as e:
			logger.warning("Couldn't load plugin %s: %s", hook_fn, e, exc_info=True)
			continue

		try:
			code = compile(code_data, name, "exec")
		except Exception as e:
			logger.warning("Couldn't compile plugin %s: %s", hook_fn, e, exc_info=True)
			continue

		mod = types.ModuleType(name)
		mod.__file__ = hook_path
		try:
			exec(code, mod.__dict__)
		except Exception as e:
			logger.warning("Couldn't exec plugin %s: %s", hook_fn, e, exc_info=True)
			continue

		if tool == "ssh":
			# Legacy
			func = getattr(mod, "hook_add_commandline_options", None)
			if func is not None:
				cmd_ = mod.hook_add_commandline_options(tool, args)
				if cmd_ is None:
					continue
				cmd = cmd_
				break

		func = getattr(mod, "hook_update_args", None)
		if func is not None:
			try:
				updated = func(tool, args)
			except Exception as e:
				logger.warning("Exception when calling %s/%s: %s",
				 name, "hook_update_args", e, exc_info=True)
				continue

			if not updated:
				continue
			cmd = [sys.argv[0]] + unparse(tool, args)
			break

	cmdline = list2cmdline(cmd)
	if caller_signature == signature(cmd):
		real_tool = find_next_program(tool)
		logger.debug("Recursed to same -> calling real SSH: %s %s", real_tool, tool_args)
		os.execv(real_tool, cmd)
	else:
		os.environ["SSH_WRAPPY_SIGNATURE"] = signature(cmd)
		logger.debug("Calling: %s", cmdline)
		os.execvp(tool, cmd)


if __name__ == '__main__':

	try:
		res = main()
	except Exception as e:
		logger.exception("Unexpected error: %s", e)
		res = 255

	raise SystemExit(res)
